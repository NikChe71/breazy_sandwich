# Breazy Sandwich #

### What is this repository for? ###

* Boilerplate for basic API's on FastAPI 

### How to set up ###

To run project, docker and docker-compose should be installed.\
To install on ubuntu - look: `./build/prepare_ubuntu.sh`

How to run:
```sh
$ sudo apt-get install git make
$ git clone https://NikChe71@bitbucket.org/NikChe71/breazy_sandwich.git
$ git checkout basic-api
$ make image run 
```

Do not forget to configure firewall. `8000` port should be opened.