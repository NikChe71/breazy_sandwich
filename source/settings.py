from os import environ
from dotenv import load_dotenv


res = load_dotenv('../variables.env')

USER = environ.get('MYSQL_USER', default='root')
PASSWORD = environ.get('MYSQL_PASSWORD', default='secret')
HOST = environ.get('MYSQL_HOST', default='0.0.0.0')
PORT = environ.get('MYSQL_PORT', default=3306)
DB = environ.get('MYSQL_DATABASE', default='test')

DSN = f"mysql://{USER}:{PASSWORD}@{HOST}:{PORT}/{DB}"
