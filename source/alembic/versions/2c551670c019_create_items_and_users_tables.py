"""create items and users tables

Revision ID: 2c551670c019
Revises: 
Create Date: 2020-10-05 23:22:25.371010

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '2c551670c019'
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        'users',
        sa.Column('id', sa.Integer, primary_key=True),
        sa.Column('email', sa.String(50), nullable=False),
        sa.Column('hashed_password', sa.String(50), nullable=False),
        sa.Column('is_active', sa.Boolean),
    )
    op.create_table(
        'items',
        sa.Column('id', sa.Integer, primary_key=True),
        sa.Column('title', sa.String(250), nullable=True),
        sa.Column('description', sa.Unicode(200)),
        sa.Column('owner_id', sa.Integer, sa.ForeignKey('users.id')),
    )


def downgrade():
    op.drop_table('items')
    op.drop_table('users')
