from typing import Optional, List

from pydantic import BaseModel
from sqlalchemy.orm import Session
from fastapi import Depends
from fastapi import APIRouter

from source import models
from source.schemas.users import User
from source.database.config import SessionLocal


router = APIRouter()


# Dependency
def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()


class Item(BaseModel):
    name: str
    description: Optional[str] = None
    price: float
    tax: Optional[float] = None


class ItemView:
    @router.get("/items/")
    async def read_item(self):
        return {"items": {"name": "Test", "description": "test descr"}}

    @router.post("/items/")
    async def create_item(self, item: Item):
        return item

    def get_users(self, db: Session, skip: int = 0, limit: int = 100):
        return db.query(models.User).offset(skip).limit(limit).all()

    @router.get("/users/", response_model=List[User])
    def read_users(self, skip: int = 0, limit: int = 100, db: Session = Depends(get_db)):
        users = self.get_users(db, skip=skip, limit=limit)
        return users
