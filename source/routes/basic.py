from fastapi import APIRouter

router = APIRouter()


class BasicView:
    @staticmethod
    @router.get("/")
    async def basic():
        return {"message": "Hello World"}

    @staticmethod
    @router.post("/")
    async def test_request(request):
        return request
