import aiomysql
import asyncio
import sys

from source.settings import USER, PASSWORD, HOST, PORT, DB


async def try_to_connect():
    print(f"Connecting to database")
    for i in range(10):
        try:
            conn = await aiomysql.connect(host=HOST, port=PORT,
                                          user=USER, password=PASSWORD,
                                          db=DB, loop=loop)
            conn.close()
            sys.exit(0)
        except Exception as e:
            print(f"Connection could not be established due to error: {e}. Waiting...")
            await asyncio.sleep(5)
    sys.exit(1)

if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    loop.run_until_complete(try_to_connect())
