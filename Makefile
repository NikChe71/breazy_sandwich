image:
	docker build -f ./build/Dockerfile . -t sandwich:latest
run:
	docker-compose -f ./build/docker-compose.yml up
clean:
	docker-compose -f ./build/docker-compose.yml down
	docker-compose -f ./build/docker-compose.yml rm -s -v -f